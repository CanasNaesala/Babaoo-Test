﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public int id;
    public GameplayManager game = default;
    // Start is called before the first frame update

    private IEnumerator Movement(Vector2 pos)
    {
        Vector3 position = new Vector3((pos.x - 1) * 1.2f, (1 - pos.y) * 1.2f, 0);
        for (int i = 1; i < 11; i++)
        {
            transform.position = Vector3.Lerp(transform.position, position, i / 10.0f);
            yield return new WaitForEndOfFrame();
        }
    }

    public void Move()
    {
        Vector2 result;
        if (game.Move(id, out result))
        {
            StartCoroutine(Movement(result));
        }
    }
}
