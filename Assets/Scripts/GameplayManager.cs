﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    [SerializeField]
    private Sprite[] androidSprite = default;
    [SerializeField]
    private Sprite[] iosSprite = default;
    [SerializeField]
    private int size = 3;
    [SerializeField]
    GameObject prefabTile = default;

    private Puzzle15 puzzle;
	private GameObject[] listTile;
    private Sprite[] usedSprite;

    void Awake()
    {
        puzzle = Puzzle15.Generate(size, new Vector2(1, 1));
#if UNITY_IOS
        usedSprite = iosSprite;
#else
        usedSprite = androidSprite;
#endif

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                int index = puzzle[i, j];
                if (index != 0)
                {
                    GameObject tile = (GameObject)Instantiate(prefabTile, new Vector3((i - 1) * 1.2f, (1 - j) * 1.2f, 0), prefabTile.transform.rotation);
                    tile.transform.parent = transform;
                    tile.GetComponent<SpriteRenderer>().sprite = usedSprite[index];
                    tile.GetComponent<Tile>().game = this;
                    tile.GetComponent<Tile>().id = index;
                }
            }
        }
    }

    public bool Move(int index, out Vector2 result)
    {
        bool test = puzzle.Move(index, out result);
        if (test == true)
        {
            if (puzzle.IsSolved())
            {
                GameObject tile = (GameObject)Instantiate(prefabTile, new Vector3(0, 0, 0), prefabTile.transform.rotation);
                tile.transform.parent = transform;
                tile.GetComponent<SpriteRenderer>().sprite = usedSprite[0];
                tile.GetComponent<Tile>().game = this;
                tile.GetComponent<Tile>().id = 1;
            }
        }
        return test;
    }
}
