﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle15
{
	private int[] list = default;
	private int size;
	private Vector2 emptyPosition;

	public int this[int i, int j]
	{
		get => list[i + j * size];
	}

	private Puzzle15()
	{
	}

	///<summary>
	///Generate a solvable solution of a given size
	///</summary>
	///<remarks>
	///0 represent the empty space, 1 to size^2 - 2 represent the squares in the readable order.
	///</remarks>
	///<param name="size">The size of the puzzle</param>
	///<param name="emptyPosition">the position of the empty space once solved, with (0, 0) as the (left, top) space</param>
	public static Puzzle15 Generate(int size, Vector2 emptyPosition)
	{
		Puzzle15 puzzle = new Puzzle15();
		puzzle.list = new int[size * size];
		puzzle.size = size;
		puzzle.emptyPosition = emptyPosition;
		int lenght = size * size;

		for (int i = 0; i < lenght; i++)
		{
			puzzle.list[i] = i;
		}

		puzzle.Randomize();

		return puzzle;
	}

	public void Randomize()
	{
		bool test = false;
		int lenght = size * size;

		while (test == false)
		{
			for (int i = 0; i < lenght - 1; i++)
			{
				int j = UnityEngine.Random.Range(i, lenght);
				if (i != j)
				{
					int temp = list[i];
					list[i] = list[j];
					list[j] = temp;
				}
			}

			test = IsSolvable();
		}
	}

	public bool Move(Vector2 tile, out Vector2 result)
	{
		int indexEmpty = Array.IndexOf(list, 0);
		Vector2 empty = new Vector2(indexEmpty % size, indexEmpty / size);
		result = empty;
		if (tile.x == empty.x && (tile.y == empty.y + 1 || tile.y == empty.y - 1) ||
			tile.y == empty.y && (tile.x == empty.x + 1 || tile.x == empty.x - 1))
		{
			list[indexEmpty] = list[(int)tile.x + (int)tile.y * size];
			list[(int)tile.x + (int)tile.y * size] = 0;
			return true;
		}
		return false;
	}

	public bool Move(int id, out Vector2 result)
	{
		int index = Array.IndexOf(list, id);
		return Move(new Vector2(index % size, index / size), out result) ;
	}

	public bool IsSolved()
	{
		int i = 0;
		int count = 1;
		int lenght = size * size;
		while (i < lenght)
		{
			if (list[i] == 0)
			{
				if (i != (int)emptyPosition.x + (int)emptyPosition.y * size)
				{
					return false;
				}
				i++;
			}
			if (count != list[i])
			{
				return false;
			}
			i++;
			count++;
		}
		list[Array.IndexOf(list, 0)] = 1;
		return true;
	}

	private bool IsSolvable()
	{
		int nbPermutation = 0;
		int lenght = size * size;

		for (int i = 0; i < lenght - 1; i++)
		{
			for (int j = i + 1; j < lenght; j++)
			{
				if (list[i] != 0 && list[j] != 0 && list[i] > list[j])
				{
					nbPermutation++;
				}
			}
		}

		//if horizontal size is odd, then the number of permutation must be even
		//if horizontal size is even, then the number of permutation must be of the same parity of the vertical space between the actual position of the empty space and the desired one.
		return (nbPermutation + (size - 1) * (emptyPosition.y + Array.IndexOf(list, 0) / size)) % 2 == 0;
	}
}
